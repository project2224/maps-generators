from setuptools import setup
setup(
   name='maps-generator',
   version='0.0.0',
   author='N/A',
   author_email='steven@electoralist.org',
   packages=['maps-generators'], 
   scripts=['run_map_generator'], 
   url='N/A',
   license='N/A',
   description='Generate GeoJSON for maps from state-provided files',
   long_description=open('README.md').read(),
)
